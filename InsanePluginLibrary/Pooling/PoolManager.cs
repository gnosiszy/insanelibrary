﻿using System;
using System.Collections.Generic;

namespace InsanePluginLibrary.Pooling
{
	/// <summary>
	/// This is a simple-but-effective way to create a MonoBehaviour pooling system.
	/// The object is instantiated and destroyed as needed.
	/// </summary>
	/// <typeparam name="T">The type of the object to be pooled</typeparam>
	public class PoolManager<T> where T : UnityEngine.MonoBehaviour, IDisposable
	{
		/// <summary>
		/// The list of objects available for usage.
		/// </summary>
		public List<T> availableObjects;

		/// <summary>
		/// The list of objects currently being used.
		/// </summary>
		public List<T> usedObjects;

		/// <summary>
		/// The original Prefab which will be used as source for new instances.
		/// </summary>
		public T prefabInstance;

		/// <summary>
		/// Clear lists and references.
		/// </summary>
		/// <param name="destroyObjects">If set to true, will also destroy instantiated objects.</param>
		public void Dispose(bool destroyObjects = true)
		{
			if (destroyObjects)
			{
				PurgeForce();
			}

			availableObjects.Clear();
			availableObjects = null;

			usedObjects.Clear();
			usedObjects = null;

			prefabInstance = null;
		}

		/// <summary>
		/// Creates a manager instance.
		/// </summary>
		/// <param name="prefabReference"></param>
		public PoolManager(T prefabReference)
		{
			availableObjects = new List<T>();
			usedObjects = new List<T>();
			prefabInstance = prefabReference;
		}

		/// <summary>
		/// Grabs or creates an instance.
		/// </summary>
		/// <returns></returns>
		public T GetInstance(bool autoSetActive = true)
		{
#if DEBUG
			string classType = typeof(T).ToString();
			UnityEngine.Debug.LogFormat("Requesting a {1} instance. Pool object count: {0}", availableObjects.Count, classType);
#endif

			T instance = availableObjects.Count > 0 ? availableObjects[0] : UnityEngine.Object.Instantiate(prefabInstance);

			if (availableObjects.Contains(instance))
			{
				availableObjects.RemoveAt(0);
			}

			usedObjects.Add(instance);
			if (autoSetActive)
			{
				instance.gameObject.SetActive(true);
			}
			return instance;
		}

		/// <summary>
		/// Returns the instance to the availableObjects pool.
		/// </summary>
		/// <param name="instance"></param>
		public void ReturnInstance(T instance)
		{
#if DEBUG
			UnityEngine.Debug.LogFormat("Returning a {0} instance to pool.", typeof(T).ToString());
#endif

			if (usedObjects.Contains(instance))
			{
				instance.gameObject.SetActive(false);
				usedObjects.Remove(instance);
				availableObjects.Add(instance);
			}
			else
			{
				UnityEngine.Debug.LogWarningFormat("This object ({0}) is not part of the pool.", typeof(T).ToString());
			}

#if DEBUG
			//UnityEngine.Debug.LogFormat("Returning done.");
#endif
		}

		/// <summary>
		/// Destroys a specific amount of instances and clear items from the pooling list. This overload does not destroyes objects in use.
		/// </summary>
		/// <param name="amount">The amount of idle instances to be purged.</param>
		public void Purge(int amount = -1)
		{
#if DEBUG
			UnityEngine.Debug.LogFormat("Purging cached objects...");
#endif

			if (availableObjects.Count == 0 || amount <= 0)
			{
				if (usedObjects.Count > 0)
				{
					UnityEngine.Debug.LogWarningFormat("No objects available for purging, however, there are {0} objects still in use.", usedObjects.Count);
				}
				return;
			}

			if (availableObjects.Count < amount)
			{
				PerformFullPurge();
			}
			else if (availableObjects.Count > 0)
			{
				for (int i = 0; i < amount; i++)
				{
					UnityEngine.Object.DestroyImmediate(availableObjects[0]);
				}
				availableObjects.RemoveRange(0, amount);
			}

#if DEBUG
			UnityEngine.Debug.LogFormat("Purging done.");
#endif
		}

		/// <summary>
		/// Destroys all instances and clear items from the pooling list. This overload does not destroyes objects in use.
		/// </summary>
		public void Purge()
		{
#if DEBUG
			UnityEngine.Debug.LogFormat("Purging cached objects...");
#endif

			PerformFullPurge();

#if DEBUG
			UnityEngine.Debug.LogFormat("Purging done.");
#endif
		}

		private void PerformFullPurge()
		{
			foreach (var _instance in availableObjects)
			{
				UnityEngine.Object.DestroyImmediate(_instance);
			}
			availableObjects.Clear();
		}

		/// <summary>
		/// USE THIS CAREFULLY: Destroys all instances and clear items from the pooling list, including objects in use.
		/// </summary>
		public void PurgeForce()
		{
#if DEBUG
			UnityEngine.Debug.LogFormat("Forcing Purge of cached objects...");
#endif

			PerformFullPurge();

			foreach (var _instance in usedObjects)
			{
				UnityEngine.Object.DestroyImmediate(_instance);
			}
			usedObjects.Clear();

#if DEBUG
			UnityEngine.Debug.LogFormat("Purging done.");
#endif
		}
	}
}