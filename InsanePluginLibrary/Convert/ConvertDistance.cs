﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

namespace InsanePluginLibrary.Convert
{
	/// <summary>
	/// Main class for Distance conversion
	/// </summary>
	public class ConvertDistance
	{
		#region Feet

		/// <summary>
		/// Converts Inch units to Foot units
		/// </summary>
		static public Foot ToFoot(Inch inch)
		{
			return inch / 12f;
		}

		/// <summary>
		/// Converts Meter units to Foot units
		/// </summary>
		static public Foot ToFoot(Meter meter)
		{
			return meter * 3.280839f;
		}

		/// <summary>
		/// Converts Mile units to Foot units
		/// </summary>
		static public Foot ToFoot(Mile mile)
		{
			return mile * 5280f;
		}

		/// <summary>
		/// Converts Yard units to Foot units
		/// </summary>
		static public Foot ToFoot(Yard yard)
		{
			return yard * 3f;
		}

		#endregion Feet

		#region Inches

		/// <summary>
		/// Converts Foot units to Inch units
		/// </summary>
		static public Inch ToInch(Foot foot)
		{
			return foot * 12f;
		}

		/// <summary>
		/// Converts Meter units to Inch units
		/// </summary>
		static public Inch ToInch(Meter meter)
		{
			return meter * 39.37007f;
		}

		/// <summary>
		/// Converts Mile units to Inch units
		/// </summary>
		static public Inch ToInch(Mile mile)
		{
			return mile * 63360;
		}

		/// <summary>
		/// Converts Yard units to Inch units
		/// </summary>
		static public Inch ToInch(Yard yard)
		{
			return yard * 36f;
		}

		#endregion Inches

		#region Meters

		/// <summary>
		/// Converts Foot units to Meter units
		/// </summary>
		static public Meter ToMeter(Foot foot)
		{
			return foot * 0.3048f;
		}

		/// <summary>
		/// Converts Inch units to Meter units
		/// </summary>
		static public Meter ToMeter(Inch inch)
		{
			return inch * 0.0254f;
		}

		/// <summary>
		/// Converts Mile units to Meter units
		/// </summary>
		static public Meter ToMeter(Mile mile)
		{
			return mile * 1609.344f;
		}

		/// <summary>
		/// Converts Yard units to Meter units
		/// </summary>
		static public Meter ToMeter(Yard yard)
		{
			return yard * 0.9144f;
		}

		#endregion Meters

		#region Miles

		/// <summary>
		/// Converts Foot units to Mile units
		/// </summary>
		static public Mile ToMile(Foot foot)
		{
			return foot / 5280f;
		}

		/// <summary>
		/// Converts Inch units to Mile units
		/// </summary>
		static public Mile ToMile(Inch inch)
		{
			return inch / 63360f;
		}

		/// <summary>
		/// Converts Meter units to Mile units
		/// </summary>
		static public Mile ToMile(Meter meter)
		{
			return meter * 6.213711f * UnityEngine.Mathf.Pow(10, -4);
		}

		/// <summary>
		/// Converts Yard units to Mile units
		/// </summary>
		static public Mile ToMile(Yard yard)
		{
			return yard / 1760f;
		}

		#endregion Miles

		#region Yards

		/// <summary>
		/// Converts Foot units to Yard units
		/// </summary>
		static public Yard ToYard(Foot foot)
		{
			return foot / 3f;
		}

		/// <summary>
		/// Converts Inch units to Yard units
		/// </summary>
		static public Yard ToYard(Inch inch)
		{
			return inch / 36f;
		}

		/// <summary>
		/// Converts Meter units to Yard units
		/// </summary>
		static public Yard ToYard(Meter meter)
		{
			return meter * 1.093613f;
		}

		/// <summary>
		/// Converts Mile units to Yard units
		/// </summary>
		static public Yard ToYard(Mile mile)
		{
			return mile * 1760f;
		}

		#endregion Yards
	}

	#region Struct ValueTypes

	/// <summary>
	/// Foot distance unit struct
	/// </summary>
	public struct Foot
	{
		private float value;

		private Foot(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Foot unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Foot distance</param>
		public static implicit operator Foot(float value)
		{
			return new Foot(value);
		}

		/// <summary>
		/// Returns the float value of a Foot distance
		/// </summary>
		/// <param name="foot">The Foot distance object</param>
		public static implicit operator float(Foot foot)
		{
			return foot.value;
		}

		/// <summary>
		/// Returns a string representation of the Foot distance value
		/// </summary>
		/// <returns>A string representation of the Foot distance value</returns>
		public override string ToString()
		{
			return string.Format("Foot ({0})", value.ToString());
		}
	}

	/// <summary>
	/// Inch distance unit struct
	/// </summary>
	public struct Inch
	{
		private float value;

		private Inch(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Foot unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Inch distance</param>
		public static implicit operator Inch(float value)
		{
			return new Inch(value);
		}

		/// <summary>
		/// Returns the float value of a Inch distance
		/// </summary>
		/// <param name="inch">The Inch distance object</param>
		public static implicit operator float(Inch inch)
		{
			return inch.value;
		}

		/// <summary>
		/// Returns a string representation of the Inch distance value
		/// </summary>
		/// <returns>A string representation of the Inch distance value</returns>
		public override string ToString()
		{
			return string.Format("Inch ({0})", value.ToString());
		}
	}

	/// <summary>
	/// Meter distance unit struct
	/// </summary>
	public struct Meter
	{
		private float value;

		private Meter(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Meter unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Meter distance</param>
		public static implicit operator Meter(float value)
		{
			return new Meter(value);
		}

		/// <summary>
		/// Returns the float value of a Meter distance
		/// </summary>
		/// <param name="meter">The Meter distance object</param>
		public static implicit operator float(Meter meter)
		{
			return meter.value;
		}

		/// <summary>
		/// Returns a string representation of the Meter distance value
		/// </summary>
		/// <returns>A string representation of the Meter distance value</returns>
		public override string ToString()
		{
			return string.Format("Meter ({0})", value.ToString());
		}
	}

	/// <summary>
	/// Mile distance unit struct
	/// </summary>
	public struct Mile
	{
		private float value;

		private Mile(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Meter unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Mile distance</param>
		public static implicit operator Mile(float value)
		{
			return new Mile(value);
		}

		/// <summary>
		/// Returns the float value of a Mile distance
		/// </summary>
		/// <param name="mile">The Mile distance object</param>
		public static implicit operator float(Mile mile)
		{
			return mile.value;
		}

		/// <summary>
		/// Returns a string representation of the Mile distance value
		/// </summary>
		/// <returns>A string representation of the Mile distance value</returns>
		public override string ToString()
		{
			return string.Format("Mile ({0})", value.ToString());
		}
	}

	/// <summary>
	/// Yard distance unit struct
	/// </summary>
	public struct Yard
	{
		private float value;

		private Yard(float value)
		{
			this.value = value;
		}

		/// <summary>
		/// Creates a new instance of Yard unit from a float value
		/// </summary>
		/// <param name="value">The float value correspondent to the Yard distance</param>
		public static implicit operator Yard(float value)
		{
			return new Yard(value);
		}

		/// <summary>
		/// Returns the float value of a Yard distance
		/// </summary>
		/// <param name="yard">The Yard distance object</param>
		public static implicit operator float(Yard yard)
		{
			return yard.value;
		}

		/// <summary>
		/// Returns a string representation of the Yard distance value
		/// </summary>
		/// <returns>A string representation of the Yard distance value</returns>
		public override string ToString()
		{
			return string.Format("Yard ({0})", value.ToString());
		}
	}

	#endregion Struct ValueTypes
}