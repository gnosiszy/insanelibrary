﻿// InsaneLog_LOCAL
//#define LOG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace InsanePluginLibrary.Log
{
	/// <summary>
	/// This class provides a way to Log information and retreive it later through a bunch of useful methods.
	/// When combined with its editor, delivers a better debugging experience, using features such as filtering, Log timestamps, Stack navigation and more.
	/// </summary>
	public class InsaneLog
	{
		/// <summary>
		/// The Level of a Log instance.
		/// </summary>
		public enum LogLevel
		{
			/// <summary>
			/// Basic information
			/// </summary>
			INFO,

			/// <summary>
			/// Warnings
			/// </summary>
			WARNING,

			/// <summary>
			/// Errors and Exceptions
			/// </summary>
			ERROR
		}

		/// <summary>
		/// This event is dispatched whenever a Log action is taken.
		/// </summary>
		static public event Action<LogData> OnLog = delegate { };

		/// <summary>
		/// Will the Stack Trace be performed at the logging method?
		/// </summary>
		static public bool LogStackTrace = true;

		/// <summary>
		/// The Lock object
		/// </summary>
		static private UnityEngine.Object insaneLock = new UnityEngine.Object();

		/// <summary>
		/// This class contains the information related to a Insane Log.
		/// </summary>
		public class LogData
		{
			private UnityEngine.Object keylock = new UnityEngine.Object();

			/// <summary>
			/// The Level of this Log instance.
			/// </summary>
			public LogLevel level;

			private static List<string> _groups;

			/// <summary>
			/// The list of all current groups
			/// </summary>
			static public List<string> Groups
			{
				get
				{
					if (_groups == null)
					{
						_groups = new List<string>();
					}
					return _groups;
				}
			}

			private static List<string> _files;

			/// <summary>
			/// The list of all logged files
			/// </summary>
			static public List<string> Files
			{
				get
				{
					if (_files == null)
					{
						_files = new List<string>();
					}
					return _files;
				}
			}

			/// <summary>
			/// The timestamp of the current Log instance
			/// </summary>
			public DateTime Time
			{
				get; internal set;
			}

			private string _file;

			/// <summary>
			/// The file path for target log
			/// </summary>
			public string File
			{
				get
				{
					return _file;
				}
				set
				{
					lock (keylock)
					{
						if (!Files.Contains(value))
						{
							Files.Add(value);
						}
						_file = value;
					}
				}
			}

			private string _group;

			/// <summary>
			/// The group of this Log instance
			/// </summary>
			public string Group
			{
				get
				{
					return _group;
				}
				set
				{
					lock (keylock)
					{
						if (!Groups.Contains(value))
						{
							Groups.Add(value);
						}
						_group = value;
					}
				}
			}

			/// <summary>
			/// The text of this Log instance
			/// </summary>
			public string Text
			{
				get; internal set;
			}

			private StackTrace _stackTrace;

			/// <summary>
			/// The stack trace of this Log instance
			/// </summary>
			public StackTrace StackTrace
			{
				get
				{
					return _stackTrace;
				}
				set
				{
#if DEBUG
					try
					{
#endif
					lock (keylock)
					{
						string filename = value.GetFrame(2).GetFileName();

						if (Application.platform == RuntimePlatform.WindowsEditor)
						{
							if (filename.Contains("\\"))
							{
								File = filename.Substring(filename.LastIndexOf('\\') + 1);
							}
						}
						else if (Application.platform == RuntimePlatform.OSXEditor)
						{
							if (filename.Contains("/"))
							{
								File = filename.Substring(filename.LastIndexOf('/') + 1);
							}
						}

						_stackTrace = value;
					}
#if DEBUG
					}
					catch (System.Exception exp)
					{
						UnityEngine.Debug.LogException(exp);
					}
#endif
				}
			}
		}

		static private List<LogData> _dataList;

		/// <summary>
		/// The list containing all current log information
		/// </summary>
		static public List<LogData> DataList
		{
			get
			{
				if (_dataList == null)
				{
					_dataList = new List<LogData>();
				}
				return _dataList;
			}
		}

		#region Logging Methods

		/// <summary>
		/// Writes a Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void Log(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.Log, group);
		}

		/// <summary>
		/// Writes a formatted Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.Log, null);
		}

		/// <summary>
		/// Writes a formatted Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.Log, group);
		}

		/// <summary>
		/// Writes a Warning Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void LogWarning(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.LogWarning, group, LogLevel.WARNING);
		}

		/// <summary>
		/// Writes a formatted Warning Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogWarningFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogWarning, null, LogLevel.WARNING);
		}

		/// <summary>
		/// Writes a formatted Warning Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogWarningFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogWarning, group, LogLevel.WARNING);
		}

		/// <summary>
		/// Writes a Error Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void LogError(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Error Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogErrorFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, null, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Error Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogErrorFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a Exception Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void LogException(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Exception Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogExceptionFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, null, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Exception Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogExceptionFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		#endregion Logging Methods

		static private void PerformLog(object info, System.Action<object> logMethod, string group = null, LogLevel level = LogLevel.INFO)
		{
#if DEBUG
			try
			{
#endif
			lock (insaneLock)
			{
				string log = (info == null) ? "null" : info.ToString();

				LogData logData = new LogData();
				logData.Text = log;

				if (Threading.MainThread.IsOnMainThread())
				{
					LogStackTrace = PlayerPrefs.GetInt("InsaneLog_StackTrace", 1) == 1;
				}

				if (LogStackTrace)
				{
					logData.StackTrace = new StackTrace(true);
				}

				logData.Group = group;
				logData.Time = DateTime.Now;

				logData.level = level;

				DataList.Add(logData);

				OnLog(logData);
			}
#if DEBUG
			}
			catch (System.Exception exp)
			{
				UnityEngine.Debug.LogException(exp);
			}
#endif
		}
	}
}