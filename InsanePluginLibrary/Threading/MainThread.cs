﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace InsanePluginLibrary.Threading
{
	/// <summary>
	/// This enum dictates the delay type. Seconds for frame-independant delay value and Frames for frame-dependant delay value.
	/// </summary>
	public enum DelayType
	{
		/// <summary>
		/// Frame-independant delay type
		/// </summary>
		Seconds,

		/// <summary>
		/// Frame-dependant delay type
		/// </summary>
		Frames
	}

	/// <summary>
	/// This class manages queues and run methods on Unity's main thread. Useful when you need to go back to the main thread from a different thread.
	/// Please keep in mind that it takes one update loop for the call to be executed. Main thread calls can be also be delayed.
	/// </summary>
	public static class MainThread
	{
		/// <summary>
		/// Debug String
		/// </summary>
		private const string classText = "[InsaneLibrary::MainThread] ";

		/// <summary>
		/// The MonoBehaviour used for synchronizing the calls with the main thread.
		/// </summary>
		static public MainThreadCallHelper Helper;

		/// <summary>
		/// Used to initialize only once.
		/// </summary>
		static public bool initialized { get; private set; }

		/// <summary>
		/// The minimum level of information which will be logged.
		/// </summary>
		static public DebugLevel DebugLevel = DebugLevel.FULL;

		/// <summary>
		/// Uses try-catch statement to invoke methods.
		/// Beware: ThreadedCallData with ForceSafeMode set to true will use safemode regardless of this option.
		/// </summary>
		static public bool SafeMode = false;

		/// <summary>
		/// The main thread helper GameObject.
		/// </summary>
		static private GameObject mainThreadHelper;

		/// <summary>
		/// The list of methods added using the Call method.
		/// </summary>
		static private List<ThreadCallData> createdActions;

		/// <summary>
		/// The list of actions which will be executed in the current frame
		/// </summary>
		static internal List<ThreadCallData> immediateActions;

		/// <summary>
		/// The list of methods which will be removed from the Immediate list.
		/// </summary>
		static private List<ThreadCallData> canceledActions;

		/// <summary>
		/// The list of actions which will be removed after iterating though the list; Used to avoid modifying the lists while others are reading it.
		/// </summary>
		static private List<ThreadCallData> toRemoveActions;

		/// <summary>
		/// Initialize this instance. It's required to use the threaded calls.
		/// </summary>
		static public void Initialize()
		{
			if (!initialized)
			{
				if (IsOnMainThread())
				{
					mainThreadHelper = new GameObject("_MainThreadCallHelper");
					Helper = mainThreadHelper.AddComponent<MainThreadCallHelper>();

					immediateActions = new List<ThreadCallData>();
					createdActions = new List<ThreadCallData>();
					canceledActions = new List<ThreadCallData>();
					toRemoveActions = new List<ThreadCallData>();

					initialized = true;
				}
#if DEBUG
				else
				{
					if (DebugLevel.Contains(DebugLevel.ERROR))
					{
						Debug.LogError(classText + "MainThread.Initialize() must be called from the Main Thread. The Initialization invocation is recomended at the Awake() or Start() methods of a MonoBehaviour.");
					}
				}
#endif
			}
		}

		/// <summary>
		/// Identifies if the current thread is the Main Thread.
		/// </summary>
		/// <returns>True if the current thread is the Main Thread</returns>
		public static bool IsOnMainThread()
		{
			//#if DEBUG
			// Internal debugging
			//Debug.Log(string.Format("\tApartmentState={0}", Thread.CurrentThread.GetApartmentState()));
			//Debug.Log(string.Format("\tManagedThreadId={0}", Thread.CurrentThread.ManagedThreadId));
			//Debug.Log(string.Format("\tIsBackground={0}", Thread.CurrentThread.IsBackground));
			//Debug.Log(string.Format("\tIsThreadPoolThread={0}", Thread.CurrentThread.IsThreadPoolThread));
			//#endif

			Thread currentThread = Thread.CurrentThread;

			return (
				// Apartmentstate must be Unknown, STA (single) and MTA (multi) are other threads.
				currentThread.GetApartmentState() == ApartmentState.Unknown &&
				// The "local" thread index. Zero is the application "Player", 1 is the main thread, any other is out.
				currentThread.ManagedThreadId == 1 &&
				// if it's a background thread, it's not the main thread
				!currentThread.IsBackground &&
				// if it's a managed thread, it's not the main.
				!currentThread.IsThreadPoolThread);
		}

		/// <summary>
		/// Invokes a method on the MainThread.
		/// </summary>
		static public ThreadCallData Call(Action pMethod)
		{
			return Call(new ThreadCallData(pMethod));
		}

		/// <summary>
		/// Invokes method defined on the ThreadedCallData in the MainThread.
		/// </summary>
		static public ThreadCallData Call(ThreadCallData pData)
		{
			if (initialized)
			{
#if DEBUG
				if (DebugLevel.Contains(DebugLevel.INFO))
				{
					Debug.Log(string.Format("{0}Queueing {1}", classText, pData.Method.Method.Name));
				}
#endif
				lock (immediateActions)
				{
					createdActions.Add(pData);
					immediateActions.Add(pData);
					return pData;
				}
			}
			else
			{
#if DEBUG
				if (DebugLevel.Contains(DebugLevel.ERROR))
				{
					Debug.LogError(classText + "Error: MainThread not initialized. Invoke MainThread.Initialize() first.");
				}
#endif
				return null;
			}
		}

		/// <summary>
		/// Invokes the pMethod on the MainThread after waiting a defined time.
		/// </summary>
		/// <param name="pMethod">The Method to be called after the specified delay.</param>
		/// <param name="pDelay">The delay in seconds.</param>
		/// <param name="pDelayType">The delay type: seconds or frames. Frames type will round to floor the delay value.</param>
		static public ThreadCallData Call(Action pMethod, float pDelay, DelayType pDelayType = DelayType.Seconds)
		{
			return Call(new ThreadCallData(pMethod), pDelay, pDelayType);
		}

		/// <summary>
		/// Invokes the method defined in the ThreadedCallData on the MainThread after waiting a defined time.
		/// </summary>
		/// <param name="pData">The ThreadedCallData to be invoked.</param>
		/// <param name="pDelay">The delay in seconds.</param>
		/// <param name="pDelayType">The delay type: seconds or frames. Frames type will round to floor the delay value.</param>
		static public ThreadCallData Call(ThreadCallData pData, float pDelay, DelayType pDelayType = DelayType.Seconds)
		{
			if (initialized)
			{
#if DEBUG
				if (DebugLevel.Contains(DebugLevel.INFO))
				{
					Debug.Log(string.Format("{0}Queueing ({1}) {3} delay) \"{2}\"", classText, pDelay, pData.Method.Method.Name, pDelayType.ToString()));
				}
#endif
				lock (createdActions)
				{
					createdActions.Add(pData);
					immediateActions.Add(new ThreadCallData(delegate { Helper.StartCoroutine(DelayedCall(pDelay, pData, pDelayType)); }));
					return pData;
				}
			}
			else
			{
#if DEBUG
				if (DebugLevel.Contains(DebugLevel.ERROR))
				{
					Debug.LogError(classText + "Error: MainThread not initialized. Invoke MainThread.Initialize() first.");
				}
#endif
				return null;
			}
		}

		/// <summary>
		/// Internal coroutine to be performed by MainThreadCallHelper
		/// </summary>
		static internal IEnumerator DelayedCall(float pDelay, ThreadCallData data, DelayType delayType)
		{
			switch (delayType)
			{
				case DelayType.Frames:
					int numFrames = Mathf.FloorToInt(pDelay);
					if (numFrames > 0)
					{
						while (numFrames > 0)
						{
							yield return null;
							numFrames--;
						}
					}
					break;

				case DelayType.Seconds:
					yield return new WaitForSeconds(pDelay);
					break;

				default:
					break;
			}

			lock (immediateActions)
			{
				if (!canceledActions.Contains(data))
				{
					immediateActions.Add(data);
				}
				else
				{
					canceledActions.Remove(data);
					data.OnDisposed += toRemoveActions.Add;
					data.Dispose();
				}
			}
		}

		/// <summary>
		/// Cancels a threaded call. Useful when using delayed calls.
		/// </summary>
		/// <param name="data">The ThreadedCallData object.</param>
		static public void Cancel(ThreadCallData data)
		{
			if (initialized)
			{
				if (createdActions.Contains(data))
				{
					canceledActions.Add(data);
				}
				else
				{
#if DEBUG
					if (DebugLevel.Contains(DebugLevel.WARNING))
					{
						Debug.LogWarning(string.Format("{0}The specified ThreadCallData is not in the execution list: {1}", classText, data));
					}
#endif
				}
			}
			else
			{
#if DEBUG
				if (DebugLevel.Contains(DebugLevel.ERROR))
				{
					Debug.LogError(classText + "Error: MainThread not initialized. Invoke MainThread.Initialize() first.");
				}
#endif
			}
		}

		/// <summary>
		/// Proper invoke called from MainThreadCallHelper
		/// </summary>
		static internal void Invoke(ThreadCallData data)
		{
			lock (immediateActions)
			{
				if (data != null)
				{
					if (data.Method != null)
					{
						if (!canceledActions.Contains(data))
						{
#if DEBUG
							if (DebugLevel.Contains(DebugLevel.INFO))
							{
								Debug.Log(string.Format("{0}Invoking {1}", classText, data.MethodName));
							}
#endif
							if (SafeMode || data.ForceSafeMode)
							{
								try
								{
									data.Method.Invoke();
								}
								catch (System.Exception e)
								{
									//Dispose
									data.OnDisposed += toRemoveActions.Add;
									data.Dispose();
									throw e;
								}
							}
							else
							{
								data.Method.Invoke();
							}
						}
						else
						{
							canceledActions.Remove(data);
						}
					}

					//Dispose
					data.OnDisposed += toRemoveActions.Add;
					data.Dispose();
				}
				else
				{
					toRemoveActions.Add(data);
				}
			}
		}

		/// <summary>
		/// Remove invoked methods from the invocation list.
		/// </summary>
		static internal void ClearInvokedMethods()
		{
			for (int i = 0; i < toRemoveActions.Count; i++)
			{
				if (immediateActions.Contains(toRemoveActions[i]))
				{
					immediateActions.Remove(toRemoveActions[i]);
				}
				if (createdActions.Contains(toRemoveActions[i]))
				{
					createdActions.Remove(toRemoveActions[i]);
				}
			}
			toRemoveActions.Clear();
		}
	}
}