# Insane Library #

This repository contains a bunch of things we're creating to improve our productivity. Uses Apache license.
If you want to suggest something, create a new issue using the menu to the left.

##Key Features##

**Main Thread:** Invoke methods in the main thread from any other thread. Great for going back from threaded web request responses or background jobs.

**Pool Manager:** A simple-yet-effective way to create a MonoBehaviour pooling system.

**InsaneLog:** Essentially a logger, where you can tag (group) messages, iterate over the logged information, retrieving all stack traces. When combined with its editor, delivers beter log vieweing using ordering, filtering, Stack trace navigation and more.

**Application Arguments:** Makes it easier to retrieve arguments when launching the game as "MY_GAME_NAME.exe -myProp:myValue".

**Conversions:** Converts from and to temperature(Fahrenheit, Celcius, Kelvin) , color(RGBA, HSV, HEX) and distances (feet, inches, meters, miles, yards).

**Scriptable Objects:** (Editor Only) Create ScriptableObjects from the scriptable class using the right click menu "Create > Create ScriptableObject From Script")


##Usage Samples##

**MainThread**

```
#!c#
void MyThreadedCallback()
{
	// Sample 1 - Call method on main thread
	MainThread.Call( MyMethod );
	
	// Sample 2 - you can use delegates and lambda expressions to access threaded values on the main thread.
	// Note: Instantiate only works on Unity's Main Thread, and it'll be named with the value retrieved from the other thread.
	string otherThreadValue = "myObjectName";
	MainThread.Call( () => { Instantiate(referenceObject).name = otherThreadvalue } );
	
	// Sample 3 - Methods can be invoked after some time, using delays.
	// Delays can be either set as seconds or frames.
	MainThread.Call( MyMethod, 3f);
	// or MainThread.Call( MyMethod, 3f, DelayType.Frames);
	
	// Sample 3 - Invokes can be cancelled before invoked.
	var threadedCall = MainThread.Call( MyMethod, 3f);
	threadedCall.Cancel();
}

```

**PoolManager**

```
#!c#
public class MyClass : MonoBehaviour
{
	// You need a reference to the object which will be instantiated and pooled
	[SerializeField]
	MyClassInstance sourcePrefab;

	void MyClass()
	{
		// Creating a new Pool instance
		PoolManager<MyClassInstance> pooling = new PoolManager<MyClassInstance>(sourcePrefab);

		// Instantiating a new object. If a object is available for reuse, it'll be reused.
		var instanceA = pooling.CreateInstance();

		// (...)

		// Returning the instance back to the pool (usually in the disposal method).
		pooling.ReturnInstance(instanceA);
	}
}

```

**Insane Log**

```
#!c#

void LogStuff()
{
	// Logs can be simple
	InsaneLog.Log("text to be logged");

	// Can contain groups (tags), which are useful for filtering
	InsaneLog.Log("other text to be logged", "myGroup");

	// Can be formatted
	InsaneLog.LogFormat("Insert text {0}", "here");

	// Can be formatted and grouped.
	InsaneLog.LogFormatGroup("myGroup", "Insert text {0}", "here");

	// Same Logging options as Unity's
	InsaneLog.LogWarning("warning log");
	InsaneLog.LogError("error log");
	InsaneLog.LogException(new System.Exception("My Exception"));

	// Read information back
	var data = InsaneLog.DataList[index];
	// data.File: File which dispatched the log
	// data.Group: Group which contains the log
	// data.level: The Log level this log has
	// data.StackTrace: The log's stack trace
	// data.Text: The Logged text
	// data.Time: The log time span
}
```



**Application Arguments**

```
#!c#

void MyClass()
{
	// Reads environment arguments
	ApplicationArguments.Initialize();

	string ip = "127.0.0.1"; // default
	int port = 1234; // default

	// Check for keys
	if (ApplicationArguments.ContainsKey("ip"))
	{
		// Get values
		ip = ApplicationArguments.GetValue("ip");
	}
	if (ApplicationArguments.ContainsKey("port"))
	{
		// Get values as a specific type
		port = ApplicationArguments.GetValueAs<int>("port");
	}

}
```