﻿// REMOTE
//#if UNITY_EDITOR
using InsanePluginLibrary.Log;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEditor;
using UnityEngine;

// THIS CLASS NEEDS REFACTORING

namespace InsaneEditorLibrary.Log
{
	/// <summary>
	/// This Class contains the Editor window of the InsaneLog class.
	/// </summary>
	public class InsaneLogWindow : EditorWindow
	{
		/// <summary>
		/// The Type of Logging UI to be shown
		/// </summary>
		public enum LogType
		{
			/// <summary>
			/// Default LogType shows items by chronological order.
			/// </summary>
			DEFAULT = 0,

			/// <summary>
			/// Group LogType shows items by chronological order, grouped by group names. Groups are displayed by addition order.
			/// </summary>
			GROUP = 2,

			/// <summary>
			/// File LogType shows items by chronological order, grouped by filename. Files are displayed by addition order.
			/// </summary>
			FILE = 3
		}

		static private InsaneLogWindow windowInstance;

		#region Settings

		static private LogType type = LogType.DEFAULT;
		private static bool showUnityInternal = false;

		#endregion Settings

		#region Scrolling

		static private Vector2 scrollPosition = Vector2.zero;
		private static bool focusLastLog = true;

		#endregion Scrolling

		#region Runtime Values

		private bool editorStatus;
		private bool FocusSelectedLog = true;
		private float targetLayoutWidth = 0;
		private float mainLayoutWidth = 0;
		private GUIStyle boxStyle;
		private GUIStyle clearBox;
		private InsaneLog.LogData selectedLogData;
		private static Rect selectedItemRect;
		private static Rect windowRect;
		private string filter = "";

		#endregion Runtime Values

		#region Editor Fields

		/// <summary>
		/// The object used to lock access
		/// </summary>
		private Object objectLock = new Object();

		/// <summary>
		/// The number of logs
		/// </summary>
		private int logCount;

		/// <summary>
		/// The code files count
		/// </summary>
		private int logFileCount;

		/// <summary>
		/// Currently selected Stack
		/// </summary>
		private int selectedIndex = 0;

		#endregion Editor Fields

		#region Stack Trace Fields

		/// <summary>
		/// The position of the stack trace scroll bar
		/// </summary>
		private Vector2 stackScroll;

		/// <summary>
		/// The number of frames of the current Stack trace
		/// </summary>
		private int stackTraceFrameCount;

		/// <summary>
		/// The list of Parameters of each method of the stack trace
		/// </summary>
		private System.Reflection.ParameterInfo[] drawStackParamInfo;

		#endregion Stack Trace Fields

		#region Group Fields

		/// <summary>
		/// The number if groups
		/// </summary>
		private int groupCount;

		/// <summary>
		/// The list of groups
		/// </summary>
		private List<InsaneLog.LogData> groupItems;

		#endregion Group Fields

		#region Static Getters

		private static Dictionary<string, bool> _groupState;

		private static Dictionary<string, bool> groupState
		{
			get
			{
				if (_groupState == null)
				{
					_groupState = new Dictionary<string, bool>();
				}
				return _groupState;
			}
		}

		private static Dictionary<string, bool> _fileState;

		private static Dictionary<string, bool> fileState
		{
			get
			{
				if (_fileState == null)
				{
					_fileState = new Dictionary<string, bool>();
				}
				return _fileState;
			}
		}

		#endregion Static Getters

		/// <summary>
		/// Opens a InsaneLog Window Instance
		/// </summary>
		[MenuItem("Insane/Open Log")]
		static public void OpenEditorWindow()
		{
			if (windowInstance != null)
			{
				windowInstance.Close();
			}

			windowInstance = CreateInstance<InsaneLogWindow>();
			windowInstance.minSize = new Vector2(700, 350);
			windowInstance.Show();
		}

		private void InitializeLoop()
		{
			//Initialize Params
			boxStyle = new GUIStyle(GUI.skin.textField);
			clearBox = new GUIStyle(GUI.skin.label);
			clearBox.alignment = TextAnchor.MiddleCenter;

			//Auto Close Box on Editor Status change
			bool newStatus = EditorApplication.isPlaying || EditorApplication.isUpdating || EditorApplication.isCompiling;
			if ((editorStatus && !newStatus) || (!editorStatus && newStatus))
			{
				targetLayoutWidth = 0;
			}
			editorStatus = newStatus;

			//Keep the division in the middle.
			if (targetLayoutWidth > 0)
			{
				targetLayoutWidth = position.width * 0.5f;
			}

			// EASING
			mainLayoutWidth = targetLayoutWidth;
			/*
			//Define Motion Speed
			if (EditorApplication.isPlaying)
			{
				mainLayoutWidth = Mathf.Lerp(mainLayoutWidth, targetLayoutWidth, 0.5f);
			}
			else
			{
				mainLayoutWidth = Mathf.Lerp(mainLayoutWidth, targetLayoutWidth, 0.1f);
			}
			//*/
		}

		// Main Loop
		private void OnGUI()
		{
			if (Event.current.type == EventType.KeyDown)
			{
				if (Event.current.keyCode == KeyCode.UpArrow)
				{
					selectedIndex--;
					FocusSelectedLog = true;
					return;
				}
				else if (Event.current.keyCode == KeyCode.DownArrow)
				{
					selectedIndex++;
					FocusSelectedLog = true;
					return;
				}
			}

			if (Event.current.type == EventType.ScrollWheel)
			{
				FocusSelectedLog = false;
			}

			lock (objectLock)
			{
				Draw();
			}
		}

		private void Draw()
		{
			InitializeLoop();

			GUILayout.Space(5f);

			// Editor
			EditorGUILayout.BeginHorizontal();
			{
				// Main Column
				EditorGUILayout.BeginVertical(GUILayout.Width(position.width - mainLayoutWidth));
				{
					// Top bar
					DrawTopBar();
					// Log List
					DrawLogItems();
					// Spacing
				}
				GUILayout.Space(5f);
				EditorGUILayout.EndVertical();

				// Separator Column
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.TextField("", GUILayout.Width(3f), GUILayout.Height(position.height));
				}
				EditorGUILayout.EndVertical();

				// Stack Trace Column
				EditorGUILayout.BeginVertical();
				DrawStackTraceTab();
				EditorGUILayout.EndVertical();
			}
			EditorGUILayout.EndHorizontal();

			//Force Editor Update
			Repaint();
		}

		private void DrawTopBar()
		{
			EditorGUILayout.BeginHorizontal();
			LogType oldType = type;
			if (string.IsNullOrEmpty(EditorPrefs.GetString("logType")))
			{
				EditorPrefs.SetString("logType", oldType.ToString());
			}
			LogType currentType = (LogType)System.Enum.Parse(typeof(LogType), EditorPrefs.GetString("logType"));
			type = (LogType)EditorGUILayout.EnumPopup(currentType, GUILayout.Width(80), GUILayout.Height(15f));
			if (type != oldType)
			{
				EditorPrefs.SetString("logType", type.ToString());
			}

			if (GUILayout.Button("Clear Log", GUILayout.Width(80), GUILayout.Height(15f)))
			{
				logFileCount = 0;
				logCount = 0;
				groupCount = 0;
				if (groupItems != null)
				{
					groupItems.Clear();
				}
				if (InsaneLog.LogData.Groups != null)
				{
					InsaneLog.LogData.Groups.Clear();
				}
				if (InsaneLog.LogData.Files != null)
				{
					InsaneLog.LogData.Files.Clear();
				}
				if (InsaneLog.DataList != null)
				{
					InsaneLog.DataList.Clear();
				}
				targetLayoutWidth = 0;
				selectedLogData = null;

				stackTraceFrameCount = 0;
				drawStackParamInfo = null;
			}

			// Stack Trace Logging
			{
				bool logStackTraceValue = PlayerPrefs.GetInt("InsaneLog_StackTrace", 1) == 1;
				logStackTraceValue = GUILayout.Toggle(logStackTraceValue, "Stack Trace");
				if (logStackTraceValue != InsaneLog.LogStackTrace)
				{
					InsaneLog.LogStackTrace = logStackTraceValue;
					PlayerPrefs.SetInt("InsaneLog_StackTrace", logStackTraceValue ? 1 : 0);
				}
			}

			// Follow Last Log
			{
				bool _focusLastLog = PlayerPrefs.GetInt("InsaneLog_FollowLastLog", 1) == 1;
				focusLastLog = _focusLastLog = GUILayout.Toggle(_focusLastLog, "Focus Last Log");
				PlayerPrefs.SetInt("InsaneLog_FollowLastLog", _focusLastLog ? 1 : 0);
			}

			GUILayout.FlexibleSpace();
			GUILayout.Label("Search:");
			filter = GUILayout.TextField(filter, GUILayout.Width(100));
			if (!string.IsNullOrEmpty(filter))
			{
				if (GUILayout.Button("Clear"))
				{
					filter = "";
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		private void DrawLogItems()
		{
			if (FocusSelectedLog && selectedLogData != null)
			{
				scrollPosition.y = Mathf.Lerp(scrollPosition.y, selectedItemRect.y - windowRect.height, 0.05f);
			}

			scrollPosition = GUILayout.BeginScrollView(scrollPosition);

			if (this.logCount != InsaneLog.DataList.Count && focusLastLog)
			{
				FocusSelectedLog = true;
				selectedLogData = InsaneLog.DataList[InsaneLog.DataList.Count - 1];
			}

			switch (type)
			{
				case LogType.GROUP:
					DrawItemsByGroup();
					break;

				case LogType.FILE:
					DrawItemsByFile();
					break;

				case LogType.DEFAULT:
					DrawItemsByTime();
					break;
			}

			GUILayout.EndScrollView();

			windowRect = GUILayoutUtility.GetLastRect();
		}

		/// <summary>
		/// Draws the Stack Trace Tab.
		/// </summary>
		private void DrawStackTraceTab()
		{
			if (targetLayoutWidth > 0)
			{
				// Box vars
				float targetWidth = targetLayoutWidth - 35;
				GUIStyle textStyle = new GUIStyle(GUI.skin.textField);

				// Close Button
				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button("Close", GUILayout.Width(targetWidth)))
					{
						targetLayoutWidth = 0;
						selectedLogData = null;
					}
				}
				EditorGUILayout.EndHorizontal();

				// Empty data should not be drawn.
				if (selectedLogData == null)
				{
					GUILayout.Label("Log data is null");
					return;
				}

				// Empty Stack Trace will not be drawn.
				if (selectedLogData.StackTrace == null)
				{
					GUILayout.Label("Stack trace not available");
					return;
				}

				// Selectable File Path
				EditorGUILayout.BeginHorizontal();
				{
					string text = "Filename not logged";
					if (selectedLogData.StackTrace != null)
					{
						text = selectedLogData.StackTrace.GetFrame(2).GetFileName();
					}
					float fieldHeight = textStyle.CalcHeight(new GUIContent(text), targetWidth);
					GUILayout.TextField(text, GUILayout.Width(targetWidth), GUILayout.Height(fieldHeight));
				}
				EditorGUILayout.EndHorizontal();

				// Configuration
				EditorGUILayout.BeginHorizontal();
				{
					showUnityInternal = EditorGUILayout.Toggle("Show UnityInternal", showUnityInternal);
					GUILayout.FlexibleSpace();
				}
				EditorGUILayout.EndHorizontal();

				// Stack Traces
				EditorGUILayout.BeginHorizontal();
				{
					DrawStackTrace(selectedLogData);
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		/// <summary>
		/// Draws the Stack Traces of a specific Log at the right hand side of the Log Window.
		/// </summary>
		/// <param name="data">The log to have its StackTrace drawn.</param>
		private void DrawStackTrace(InsaneLog.LogData data)
		{
			stackTraceFrameCount = data.StackTrace.FrameCount;

			EditorGUILayout.BeginVertical();
			stackScroll = EditorGUILayout.BeginScrollView(stackScroll);
			for (int i = stackTraceFrameCount - 1; i > 0; i--)
			{
				StackFrame frame = data.StackTrace.GetFrame(i);
				if (frame != null)
				{
					string filename = frame.GetFileName();

					if (string.IsNullOrEmpty(filename) || filename.StartsWith("C:\\buildslave"))
					{
						if (showUnityInternal)
						{
							EditorGUILayout.BeginHorizontal();
							{
								EditorGUI.BeginDisabledGroup(true);
								{
									GUILayout.Button(string.Format("[[ UnityInternal::{0} ]]", frame.GetMethod().Name), GUILayout.Width(targetLayoutWidth - 35));
								}
								EditorGUI.EndDisabledGroup();
							}
							EditorGUILayout.EndHorizontal();
							GUILayout.Label(@"↓", clearBox, GUILayout.Width(targetLayoutWidth - 35));
						}
					}
					else
					{
						EditorGUILayout.BeginHorizontal();
						{
							string _params = "";
							drawStackParamInfo = frame.GetMethod().GetParameters();
							if (drawStackParamInfo != null)
							{
								foreach (System.Reflection.ParameterInfo info in drawStackParamInfo)
								{
									_params += string.Format("{0}:{1}, ", info.Name, info.ParameterType.Name);
								}
								if (_params.Length > 3)
								{
									_params = _params.Substring(0, _params.Length - 2);
								}

#if UNITY_EDITOR_OSX
								string buttonCaption = string.Format("{0} at {2}({3})", frame.GetMethod().Name, _params, filename.Substring(filename.LastIndexOf('/') + 1), frame.GetFileLineNumber());
#else
								string buttonCaption = string.Format("{0} at {2}({3})", frame.GetMethod().Name, _params, filename.Substring(filename.LastIndexOf('\\') + 1), frame.GetFileLineNumber());
#endif

								if (GUILayout.Button(buttonCaption, GUILayout.Width(targetLayoutWidth - 35)))
								{
									string splitpath = "";
									if (filename.Contains("Assets"))
									{
										splitpath = filename.Substring(filename.IndexOf("Assets"));
									}
									else if (filename.Contains("corlib"))
									{
										splitpath = filename.Substring(filename.IndexOf("corlib"));
									}

									//string splitpath = filename.Substring(filename.IndexOf("Assets"));
									string fixedName = string.Join("/", splitpath.Split('\\'));

									var obj = AssetDatabase.LoadAssetAtPath(fixedName, typeof(TextAsset));
									if (obj is TextAsset || obj is ComputeShader)
									{
										AssetDatabase.OpenAsset(obj, frame.GetFileLineNumber());
									}
									else
									{
										UnityEngine.Debug.LogWarning("Could not open the file \"" + filename + "\"");
									}
								}
							}
						}
						EditorGUILayout.EndHorizontal();
						GUILayout.Label(@"↓", clearBox, GUILayout.Width(targetLayoutWidth - 35));
					}
				}
			}

			// Quote Message.
			GUIStyle stl = new GUIStyle(clearBox);
			stl.fontStyle = FontStyle.Italic;
			stl.richText = true;
			GUILayout.Label(string.Format("\"{0}\"", data.Text), stl, GUILayout.Width(targetLayoutWidth - 35));

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndVertical();
		}

		/// <summary>
		/// Draws each item in the order they were logged
		/// </summary>
		private void DrawItemsByTime()
		{
			logCount = InsaneLog.DataList.Count;

			if (InsaneLog.DataList.Contains(selectedLogData))
			{
				if (selectedIndex < 0)
				{
					int itemIndex = InsaneLog.DataList.IndexOf(selectedLogData) - 1;
					itemIndex = Mathf.Clamp(itemIndex, 0, InsaneLog.DataList.Count - 1);
					selectedLogData = InsaneLog.DataList[itemIndex];
					selectedIndex = 0;
				}
				else if (selectedIndex > 0)
				{
					int itemIndex = InsaneLog.DataList.IndexOf(selectedLogData) + 1;
					itemIndex = Mathf.Clamp(itemIndex, 0, InsaneLog.DataList.Count - 1);
					selectedLogData = InsaneLog.DataList[itemIndex];
					selectedIndex = 0;
				}
			}

			for (int i = 0; i < logCount; i++)
			{
				DrawItem(InsaneLog.DataList[i]);
			}
		}

		/// <summary>
		/// Filters Log list by Group and invokes DrawItem for each item
		/// </summary>
		private void DrawItemsByGroup()
		{
			groupCount = InsaneLog.LogData.Groups.Count;

			for (int i = 0; i < groupCount; i++)
			{
				GUILayout.BeginVertical();

				string groupName = InsaneLog.LogData.Groups[i];
				if (groupName == null)
				{
					groupName = "";
				}
				if (!groupState.ContainsKey(groupName))
				{
					groupState[groupName] = true;
				}

				groupState[groupName] = EditorPrefs.GetBool(groupName, true);
				bool previousState = groupState[groupName];

				if (string.IsNullOrEmpty(groupName))
				{
					groupState[groupName] = EditorGUILayout.BeginToggleGroup("Ungrouped", groupState[groupName]);
				}
				else
				{
					groupState[groupName] = EditorGUILayout.BeginToggleGroup(string.Format("Group \"{0}\"", groupName), groupState[groupName]);
				}

				if (previousState != groupState[groupName])
				{
					EditorPrefs.SetBool(groupName, groupState[groupName]);
				}

				//groupState[groupName] = EditorGUILayout.BeginToggleGroup(string.Format("Group \"{0}\"", groupName), groupState[groupName]);

				if (groupState[groupName])
				{
					lock (InsaneLog.DataList)
					{
						groupItems = new List<InsaneLog.LogData>(InsaneLog.DataList.Where(item => item.Group == InsaneLog.LogData.Groups[i]));

						if (groupItems != null)
						{
							if (groupItems.Contains(selectedLogData))
							{
								if (selectedIndex < 0)
								{
									int itemIndex = groupItems.IndexOf(selectedLogData) - 1;
									itemIndex = Mathf.Clamp(itemIndex, 0, groupItems.Count - 1);
									selectedLogData = groupItems[itemIndex];
									selectedIndex = 0;
								}
								else if (selectedIndex > 0)
								{
									int itemIndex = groupItems.IndexOf(selectedLogData) + 1;
									itemIndex = Mathf.Clamp(itemIndex, 0, groupItems.Count - 1);
									selectedLogData = groupItems[itemIndex];
									selectedIndex = 0;
								}
							}

							foreach (InsaneLog.LogData data in groupItems)
							{
								DrawItem(data);
							}
						}
					}
				}
				EditorGUILayout.EndToggleGroup();

				GUILayout.EndVertical();
				if (previousState)
				{
					GUILayout.Space(20f);
				}
			}
		}

		/// <summary>
		/// Filters Log list by File and invokes DrawItem for each item
		/// </summary>
		private void DrawItemsByFile()
		{
			logFileCount = InsaneLog.LogData.Files.Count;

			for (int i = 0; i < logFileCount; i++)
			{
				GUILayout.BeginVertical();

				//GUILayout.Box( string.Format("File \"{0}\"", InsaneLog.LogData.Files[i]) );
				string fileName = InsaneLog.LogData.Files[i];
				if (!fileState.ContainsKey(fileName))
				{
					fileState[fileName] = true;
				}

				fileState[fileName] = EditorPrefs.GetBool(fileName, true);
				bool previousState = fileState[fileName];

				fileState[fileName] = EditorGUILayout.BeginToggleGroup(string.Format("File \"{0}\"", fileName), fileState[fileName]);
				if (previousState != fileState[fileName])
				{
					EditorPrefs.SetBool(fileName, fileState[fileName]);
				}

				if (fileState[fileName])
				{
					List<InsaneLog.LogData> fileItems = new List<InsaneLog.LogData>(InsaneLog.DataList.Where(item => item.File == InsaneLog.LogData.Files[i]));
					if (fileItems != null)
					{
						if (fileItems.Contains(selectedLogData))
						{
							if (selectedIndex < 0)
							{
								int itemIndex = fileItems.IndexOf(selectedLogData) - 1;
								itemIndex = Mathf.Clamp(itemIndex, 0, fileItems.Count - 1);
								selectedLogData = fileItems[itemIndex];
								selectedIndex = 0;
							}
							else if (selectedIndex > 0)
							{
								int itemIndex = fileItems.IndexOf(selectedLogData) + 1;
								itemIndex = Mathf.Clamp(itemIndex, 0, fileItems.Count - 1);
								selectedLogData = fileItems[itemIndex];
								selectedIndex = 0;
							}
						}

						foreach (InsaneLog.LogData data in fileItems)
						{
							DrawItem(data);
						}
					}
				}
				EditorGUILayout.EndToggleGroup();

				if (previousState)
				{
					GUILayout.Space(20f);
				}

				GUILayout.EndVertical();
			}
		}

		/// <summary>
		/// Each individual Log Entry drawn at the lef thand side of the Log Window
		/// </summary>
		/// <param name="data"></param>
		private void DrawItem(InsaneLog.LogData data)
		{
			if (!string.IsNullOrEmpty(filter))
			{
				if (!data.Text.ToLower().Contains(filter.ToLower()))
					return;
			}

			GUIStyle _labelStyle = new GUIStyle(GUI.skin.label);
			GUIStyle _boxStyle = new GUIStyle(GUI.skin.textField);
			_boxStyle.richText = true;

			if (data == selectedLogData)
			{
				_labelStyle.fontStyle = FontStyle.Bold;
				_boxStyle.fontStyle = FontStyle.Bold;
			}

			Vector2 contentSize = boxStyle.CalcSize(new GUIContent(data.Text));
			float contentHeight = boxStyle.CalcHeight(new GUIContent(data.Text), contentSize.x);

			GUILayout.Space(5f);
			GUILayout.BeginHorizontal();
			GUILayout.Label(data.Time.ToString("mm:ss:fff"), _labelStyle, GUILayout.Width(65f), GUILayout.Height(contentHeight));

			bool jumpScrollToPosition = false;

			_boxStyle.normal.textColor = data.level == InsaneLog.LogLevel.INFO ? Color.grey : data.level == InsaneLog.LogLevel.WARNING ? Color.yellow : Color.red;
			if (GUILayout.Button(data.Text, _boxStyle, GUILayout.Height(contentHeight)))
			{
				selectedLogData = data;

				if (!FocusSelectedLog)
				{
					jumpScrollToPosition = true;
				}

				FocusSelectedLog = true;

				if (data.StackTrace != null)
				{
					targetLayoutWidth = position.width * 0.5f;
				}
			}
			GUILayout.EndHorizontal();

			if (selectedLogData == data && Event.current.type == EventType.Repaint)
			{
				selectedItemRect = GUILayoutUtility.GetLastRect();

				if (jumpScrollToPosition)
				{
					scrollPosition.y = selectedItemRect.y - windowRect.height;
				}
			}
		}
	}
}

//#endif